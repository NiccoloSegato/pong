// 
// SPACE PONG
// 
// Copyright 2017 - Ardizzoni Simone e Segato Niccolo'
//

#include "stdafx.h"
#include <time.h>
#include <stdio.h>
#include <string>
#include <Windows.h>
#include <iostream>
#include <conio.h>
#include <sstream>
#include <math.h>
#include <gl\Gl.h>
#include <gl\GLU.h>
#include "GL\freeglut.h"
#pragma comment(lib, "OpenGL32.lib")
#define VK_W 0x57		// Tasto W della tastiera
#define VK_S 0x53		// Tasto S della tastiera
#define VK_A 0x41		// Tasto A della tastiera
#define VK_D 0x44		// Tasto D della tastiera
#define VK_ENTER 0x0D	// Tasto INVIO della tastiera
#define VK_ESCAPE 0x1B	// Tasto ESC della tastiera

/***** Funzione per la conversione da INT to STRING *****/
std::string int2str(int x) {
	std::stringstream ss;
	ss << x;
	return ss.str();
}
/*********************************************************/
bool inGame = false;	// Definisce se il gioco � cominciato
bool pause = false;		// DEfinisce se il gioco � in pausa

const int width = 500;			// Larghezza della finestra
const int height = 300;			// Altezza della finestra
const int interval = 1000 / 60;	// Framerate (60FPS)

int punteggio_sx = 0;		// Punteggio iniziale giocatore di sinistra
int punteggio_dx = 0;		// Punteggio iniziale giocatore di destra

int racket_width = 10;		// Larghezza Racchetta
int racket_height = 80;		// Altezza Racchetta
int racket_speed = 5;		// Velocita Racchetta

float racket_left_x = 0;	// Posizione iniziale sulle ascisse della Racchetta Sinistra
float racket_left_y = 100.0f;	// Posizione iniziale sulle ordinate della Racchetta Sinistra

float racket_right_x = width - racket_width - 10;	// Posizione iniziale sulle ascisse della Racchetta Destra
float racket_right_y = 100;						// Posizione iniziale sulle ordinate della Racchetta Sinistra

float ball_pos_x = width / 2;	// Posizione iniziale sulle ascisse della palla
float ball_pos_y = height / 2;	// Posizione iniziale sulle ordinate della palla

int height_pitch = height - 35;		// Altezza del campo di gioco

float ball_dir_x = 1.0f;		// Direzione iniziale sulle ascisse della palla
float ball_dir_y = 0.0f;		// Direzione iniziale sulle ordinate della palla
float ball_speed = 3;			// Velocita iniziale della palla
int ball_size = 8;				// Dimensione della palla

bool conto = false;

class Rettangolo {
public:
	void drawRectangle(float x1, float x2, float y2, float y1) {
		// X1 = X minore
		// X2 = X maggiore
		// Y1 = Y maggiore
		// Y2 = Y minore

		glBegin(GL_QUADS);
		glVertex2f(x1, y1);
		glVertex2f(x1, y2);
		glVertex2f(x2, y2);
		glVertex2f(x2, y1);
		glEnd();
	}
};

class InGame {
private:
	Rettangolo rettangolo;
public:
	void vec2_norm(float& x, float &y) {
		float lenght = sqrt((x*x) + (y*y));
		if (lenght != 0.0f) {
			lenght = 1.0f / lenght;
			x *= lenght;
			y *= lenght;
		}
	}

	/********* Aumento velocita ad ogni tocco della palla con una racchetta ******/
	void aumentaVelocita() {
		if (ball_speed < 9) {
			ball_speed += 0.2;
		}
	}
	/*********************************************************************************/

	void updateRacket() {

		// La racchetta di sinistra tocca il limite superiore della finestra
		if (racket_left_y > height_pitch - racket_height) {
			racket_left_y = height_pitch - racket_height;
		}

		// La racchetta di sinistra tocca il limite inferiore della finestra
		if (racket_left_y < 0) {
			racket_left_y = 0;
		}

		// La racchetta di destra tocca il limite superiore della finestra
		if (racket_right_y > height_pitch - racket_height) {
			racket_right_y = height_pitch - racket_height;
		}

		// La racchetta di destra tocca il limite inferiore della finestra
		if (racket_right_y < 0) {
			racket_right_y = 0;
		}

		// Hit left wall
		if (racket_left_x < 0) {
			racket_left_x = 0; // force it to be positive
		}

		// Hit right wall
		if (racket_right_x > width - racket_width * 2) {
			racket_right_x = width - racket_width * 2; // force it to be positive
		}

		// Reach the middle
		if (racket_right_x < (width / 2)) {
			racket_right_x = (width / 2); // force it to be positive
		}

		// Reach the middle
		if (racket_left_x > (width / 2)-racket_width) {
			racket_left_x = (width / 2)-racket_width; // force it to be positive
		}
	}

	void updateBall() {
		// fly a bit
		ball_pos_x += ball_dir_x * ball_speed;
		ball_pos_y += ball_dir_y * ball_speed;

		// hit by left racket?
		if (ball_pos_x < racket_left_x + racket_width &&
			ball_pos_x > racket_left_x &&
			ball_pos_y < racket_left_y + racket_height &&
			ball_pos_y > racket_left_y) {
			// set fly direction depending on where it hit the racket
			// (t is 0.5 if hit at top, 0 at center, -0.5 at bottom)
			float t = ((ball_pos_y - racket_left_y) / racket_height) - 0.5f;
			ball_dir_x = fabs(ball_dir_x); // force it to be positive
			ball_dir_y = t;
			aumentaVelocita();
		}

		// hit by right racket?
		if (ball_pos_x > racket_right_x &&
			ball_pos_x < racket_right_x + racket_width &&
			ball_pos_y < racket_right_y + racket_height &&
			ball_pos_y > racket_right_y) {
			// set fly direction depending on where it hit the racket
			// (t is 0.5 if hit at top, 0 at center, -0.5 at bottom)
			float t = ((ball_pos_y - racket_right_y) / racket_height) - 0.5f;
			ball_dir_x = -fabs(ball_dir_x); // force it to be negative
			ball_dir_y = t;
			aumentaVelocita();
		}

		// hit left wall?
		if (ball_pos_x < 0) {
			++punteggio_dx;
			ball_pos_x = width / 2;
			ball_pos_y = height / 2;
			ball_dir_x = fabs(ball_dir_x); // force it to be positive
			ball_dir_y = 0;
			ball_speed = 3;
			racket_left_x = 0;
			racket_left_y = 100.0f;
			racket_right_x = width - racket_width - 10;
			racket_right_y = 100;
			conto = true;
		}

		// hit right wall?
		if (ball_pos_x > width) {
			++punteggio_sx;
			ball_pos_x = width / 2;
			ball_pos_y = height / 2;
			ball_dir_x = -fabs(ball_dir_x); // force it to be negative
			ball_dir_y = 0;
			ball_speed = 3;
			racket_left_x = 0;
			racket_left_y = 100.0f;
			racket_right_x = width - racket_width - 10;
			racket_right_y = 100;
			conto = true;
		}

		// hit top wall?
		if (ball_pos_y > height_pitch) {
			ball_dir_y = -fabs(ball_dir_y); // force it to be negative
		}

		// hit bottom wall?
		if (ball_pos_y < 1) {
			ball_dir_y = fabs(ball_dir_y); // force it to be positive
		}

		// make sure that length of dir stays at 1
		vec2_norm(ball_dir_x, ball_dir_y);
	}

	void keyboard() {
		if (GetAsyncKeyState(VK_W)) {
			racket_left_y += racket_speed;
		}
		if (GetAsyncKeyState(VK_S)) {
			racket_left_y -= racket_speed;
		}

		if (GetAsyncKeyState(VK_A)) {
			racket_left_x -= racket_speed;
		}

		if (GetAsyncKeyState(VK_D)) {
			racket_left_x += racket_speed;
		}

		if (GetAsyncKeyState(VK_UP)) {
			racket_right_y += racket_speed;
		}
		if (GetAsyncKeyState(VK_DOWN)) {
			racket_right_y -= racket_speed;
		}
		if (GetAsyncKeyState(VK_RIGHT)) {
			racket_right_x += racket_speed;
		}
		if (GetAsyncKeyState(VK_LEFT)) {
			racket_right_x -= racket_speed;
		}
		if (GetAsyncKeyState(VK_ENTER)) {
			if (inGame) {
				inGame = false;
				Sleep(150);
			}
			else {
				inGame = true;
				Sleep(150);
			}
		}
		if (GetAsyncKeyState(VK_ESCAPE)) {
			if(pause) {
				pause = false;
				Sleep(150);
			}
			else {
				pause = true;
				Sleep(150);
			}
		}
	}

	void drawRect(float x, float y, float width, float height) {
		glBegin(GL_QUADS);
		glVertex2f(x, y);
		glVertex2f(x + width, y);
		glVertex2f(x + width, y + height);
		glVertex2f(x, y + height);
		glEnd();
	}

	void drawPunteggio(float x, float y, std::string text) {
		glRasterPos2f(x, y);
		glutBitmapString(GLUT_BITMAP_8_BY_13, (const unsigned char*)text.c_str());
	}

	void drawVelocita(float x, float y, std::string text) {
		glRasterPos2f(x, y);
		glutBitmapString(GLUT_BITMAP_8_BY_13, (const unsigned char*)text.c_str());
	}

	int calcolaVelocita() {
		return (ball_speed * 100) / 9;
	}

	void drawPitch() {

		// Disegna la linea centrale
		glBegin(GL_LINES);		// Istanzio un oggetto OpenGL di tipo GL_LINES
		glVertex2f(width / 2, 0);	// Determino il punto di partenza di coordinate (width/2, 0)
		glVertex2f(width / 2, height_pitch);	// Determino il punto finale di coordinate (width/2, height)
		glEnd();

		// Disegna la linea superiore
		glBegin(GL_LINES);
		glVertex2d(0, height_pitch);
		glVertex2d(width, height_pitch);
		glEnd();
	}

	bool vittoria() {
		if (((punteggio_dx > punteggio_sx) && (punteggio_dx > 9)) || ((punteggio_sx > punteggio_dx) && (punteggio_sx > 9))) {
			return true;
		}
		else {
			return false;
		}
	}

	/*void countdown() {
		clock_t start = clock();
		while (true) {
			glBegin(GL_QUADS);
			glVertex2f(width / 2 - 25, height / 2 - 5);
			glVertex2f(width / 2 - 25, height / 2 + 5);
			glVertex2f(width / 2 + 25, height / 2 + 5);
			glVertex2f(width / 2 + 25, height / 2 - 5);
			glEnd();
			if ((((double)(clock() - start)) / CLOCKS_PER_SEC) == 1) {
				conto = false;
				break;
			}
		}
	}*/

};

class MainMenu {
private:
	Rettangolo rettangolo;
public:
	void drawTitle() {
		drawS();
		//drawP1();
		//drawA();
		//drawC();
		//drawE();

		drawP();
		drawO();
		drawN();
		drawG();

		drawHint(141, 50, "Premi ENTER per giocare");
	}

	void drawS() {

		// Parte 1 S
		rettangolo.drawRectangle(100, 115, 217, 215);

		// Parte 2 S
		rettangolo.drawRectangle(113, 115, 222, 215);

		// Parte 3 S
		rettangolo.drawRectangle(100, 115, 224, 222);

		// Parte 4 S
		rettangolo.drawRectangle(100, 102, 230, 223);
	}

	void drawP() {

		// Parte 1 P
		rettangolo.drawRectangle(100, 110, 200, 100);

		// Parte 2 P
		rettangolo.drawRectangle(110, 150, 200, 190);

		// Parte 3 P
		rettangolo.drawRectangle(110, 150, 170, 160);

		// Parte 4 P
		rettangolo.drawRectangle(140, 150, 190, 170);
	}

	void drawO() {

		// Parte 1 O
		rettangolo.drawRectangle(175, 185, 200, 100);

		// Parte 2 O
		rettangolo.drawRectangle(215, 225, 200, 100);

		// Parte 3 O
		rettangolo.drawRectangle(185, 215, 200, 190);

		// Parte 4 O
		rettangolo.drawRectangle(185, 215, 110, 100);
	}

	void drawN() {

		// Parte 1 N
		rettangolo.drawRectangle(250, 260, 200, 100);

		// Parte 2 N
		glBegin(GL_QUADS);
		glVertex2f(260, 200);
		glVertex2f(260, 175);
		glVertex2f(290, 100);
		glVertex2f(290, 125);
		glEnd();

		// Parte 3 N
		rettangolo.drawRectangle(290, 300, 200, 100);
	}

	void drawG() {

		// Parte 1 G
		rettangolo.drawRectangle(325, 335, 200, 100);

		// Parte 2 G
		rettangolo.drawRectangle(335, 375, 200, 190);

		// Parte 3 G
		rettangolo.drawRectangle(335, 375, 110, 100);

		// Parte 4 G
		rettangolo.drawRectangle(365, 375, 150, 110);

		// Parte 5 Gaaaaaa
		rettangolo.drawRectangle(355, 365, 150, 140);
	}

	void drawHint(float x, float y, std::string text) {
		glRasterPos2f(x, y);
		glutBitmapString(GLUT_BITMAP_8_BY_13, (const unsigned char*)text.c_str());
	}

};

class PauseMenu {
public:
	void drawMenu() {
		glClearColor(0.0, 0.0, 0.0, 0.0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		drawPause(225, height / 2, "PAUSA");
	}

	void drawPause(float x, float y, std::string text) {
		glRasterPos2f(x, y);
		glutBitmapString(GLUT_BITMAP_8_BY_13, (const unsigned char*)text.c_str());
	}
};

void draw() {
	InGame Game;
	MainMenu Menu;
	PauseMenu Pause;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	if (inGame) {

		if (pause) {
			Pause.drawMenu();
		}
		else {

			Game.drawRect(racket_left_x, racket_left_y, racket_width, racket_height);
			Game.drawRect(racket_right_x, racket_right_y, racket_width, racket_height);

			Game.drawRect(ball_pos_x - ball_size / 2, ball_pos_y - ball_size / 2, ball_size, ball_size);

			Game.drawPunteggio(width / 2 - 10, height - 20, int2str(punteggio_sx) + ":" + int2str(punteggio_dx));

			Game.drawVelocita(15, height - 20, "Ball Speed: " + int2str(Game.calcolaVelocita()) + "%");

			Game.drawPitch();

			if (conto) {
				//Game.countdown();
			}
		}
	}
	else {
		Menu.drawTitle();
	}
	
	glutSwapBuffers();
}

void update(int valore) {
	InGame Game;
	MainMenu Menu;

	Game.keyboard();

	Game.updateBall();
	Game.updateRacket();

	glutTimerFunc(interval, update, 0);

	glutPostRedisplay();
}

void enable2D(int width, int height) {
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0f, width, 0.0f, height, 0.0f, 1.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

int _tmain(int argc, char** argv)
{
	InGame Game;
	MainMenu Menu;

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(width, height);
	glutCreateWindow("Segato&Bobby's Pong");

	glutDisplayFunc(draw);
	glutTimerFunc(interval, update, 0);

	enable2D(width, height);
	glColor3f(1.0f, 1.0f, 1.0f);

	glutMainLoop();
    return 0;
}